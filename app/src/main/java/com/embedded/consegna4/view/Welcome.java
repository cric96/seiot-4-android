package com.embedded.consegna4.view;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.embedded.consegna4.R;
import com.embedded.consegna4.model.home.HomeFactory;
import com.embedded.consegna4.model.pattern.Message;
import com.embedded.consegna4.view.common.AbstractNotifiableActivity;
import com.embedded.consegna4.view.common.ConnectionTask;
import com.embedded.consegna4.view.common.ViewManager;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class Welcome extends AbstractNotifiableActivity {
    private final int ENABLE_BT_REQ = 1;
    private BluetoothAdapter btAdapter ;
    private boolean registered = false;
    private Set<BluetoothDevice> nbDevices = new HashSet<BluetoothDevice>();
    private final BroadcastReceiver br = new BroadcastReceiver (){
        @Override
        public void onReceive (Context context , Intent intent ){
            BluetoothDevice device = null ;
            if(BluetoothDevice.ACTION_FOUND.equals(intent.getAction())){
                device = intent.getParcelableExtra (BluetoothDevice.EXTRA_DEVICE);
                final Button welcome = Welcome.this.findViewById(R.id.discovery);
                if(device.getName().equals(getString(R.string.device))) {
                    ConnectionTask task = new ConnectionTask(device, UUID.fromString(getString(R.string.uuid)),welcome);
                    task.execute();
                }
            }
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        HomeFactory.getHome().addObserver(ViewManager.instance());
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.welcome);
        final Button welcome = this.findViewById(R.id.discovery);
        welcome.setEnabled(false);
        welcome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ViewManager.instance().changeView(OnConnection.class);
            }
        });

        btAdapter = BluetoothAdapter.getDefaultAdapter ();
        if( btAdapter == null ){
            finish ();
        }
        //RICHIEDO DI ATTIVARE IL BLUETOOTH
        if (!btAdapter.isEnabled ()){
            startActivityForResult (
                    new Intent ( BluetoothAdapter . ACTION_REQUEST_ENABLE ), ENABLE_BT_REQ );
        }
        //Mi devo registrare al servizio che fornisce connessione bluetooth come ascoltatore, ma solo una volta per ogni activity
        if(!registered) {
            registerReceiver (br , new IntentFilter( BluetoothDevice .
                    ACTION_FOUND ));
            registered = true;
        }
    }
    @Override
    public void onStart (){
        super.onStart ();
        btAdapter.startDiscovery();
    }

    public void onStop (){
        super.onStop ();
        btAdapter.cancelDiscovery ();
    }

    public void onPause() {
        super.onPause();
        btAdapter.cancelDiscovery ();
    }

    public void onDestroy() {
        super.onDestroy();
        if (registered) {
            unregisterReceiver(br);
            registered = false;
        }
    }
    @Override
    public void processMsg(Message msg) {}

    protected void homeStatusCheck() {
        if(HomeFactory.getHome().isEntered()) {
            Log.i("here...","why don't change??");
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ViewManager.instance().changeView(ControlManager.class);
                }
            });
        }
        if(HomeFactory.getHome().isLogged()) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ViewManager.instance().changeView(Enter.class);
                }
            });
        }

    }
}
