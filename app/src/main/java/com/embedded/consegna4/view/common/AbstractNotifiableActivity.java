package com.embedded.consegna4.view.common;

import android.app.Activity;
import android.os.Bundle;

import com.embedded.consegna4.model.home.HomeFactory;

/**
 * an abstract notifiable activity
 */
public abstract class AbstractNotifiableActivity extends Activity implements NotifiableView {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.homeStatusCheck();
        ViewManager.instance().changeNotifiableView(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.homeStatusCheck();
        ViewManager.instance().changeNotifiableView(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        this.homeStatusCheck();
        ViewManager.instance().changeNotifiableView(this);
    }
    /**
     * check the current state of home and decide what to do
     */
    protected abstract void homeStatusCheck();
}
