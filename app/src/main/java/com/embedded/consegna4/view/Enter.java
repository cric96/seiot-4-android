package com.embedded.consegna4.view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;

import com.embedded.consegna4.R;
import com.embedded.consegna4.model.home.HomeFactory;
import com.embedded.consegna4.model.home.InMessage;
import com.embedded.consegna4.model.pattern.Message;
import com.embedded.consegna4.view.common.AbstractNotifiableActivity;
import com.embedded.consegna4.view.common.ViewManager;

/**
 * Created by paggi on 01/03/2018.
 */

public class Enter extends AbstractNotifiableActivity {
    private AlertDialog alert;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.entering);
        //TODO METTI IN RISORSE

        alert = new AlertDialog.Builder(Enter.this).create();
        alert.setTitle(getString(R.string.enterAlertTitle));
        alert.setMessage(getString(R.string.enterAlertMsg));
        alert.setButton(Dialog.BUTTON_POSITIVE,getString(R.string.OkButton),new DialogInterface.OnClickListener(){

            @Override
            public void onClick(DialogInterface dialog, int which) {
                ViewManager.instance().changeView(Welcome.class);
                finish();
            }
        });
    }

    @Override
    public void processMsg(Message msg) {
        if(msg instanceof InMessage) {
            switch((InMessage)msg) {
                case NOT_DETECTED:
                    this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            alert.show();
                        }
                    });
                break;
                case ENTERED:
                    this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ViewManager.instance().changeView(ControlManager.class);
                        }
                    });
            }
        }
    }
    @Override
    protected void homeStatusCheck() {
        if(!HomeFactory.getHome().isLogged()) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ViewManager.instance().changeView(Login.class);
                }
            });
        }
        if(HomeFactory.getHome().isEntered()) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ViewManager.instance().changeView(ControlManager.class);
                }
            });
        }
    }
}
