package com.embedded.consegna4.view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;

import com.embedded.consegna4.R;
import com.embedded.consegna4.model.home.HomeFactory;
import com.embedded.consegna4.model.home.InMessage;
import com.embedded.consegna4.model.pattern.Message;
import com.embedded.consegna4.view.common.AbstractNotifiableActivity;
import com.embedded.consegna4.view.common.ViewManager;

public class OnConnection extends AbstractNotifiableActivity {
    private AlertDialog alert;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.on_connection);
        alert = new AlertDialog.Builder(OnConnection.this).create();
        //TODO METTI IN RISORSE
        alert.setTitle(getString(R.string.connectionDialogTitle));
        alert.setMessage(getString(R.string.connectionDialogMsg));
        alert.setButton(Dialog.BUTTON_POSITIVE,getString(R.string.OkButton),new DialogInterface.OnClickListener(){

            @Override
            public void onClick(DialogInterface dialog, int which) {
                ViewManager.instance().changeView(Welcome.class);
                finish();
            }
        });
    }

    @Override
    protected void onRestart() {
        super.onRestart();

    }

    @Override
    public void processMsg(Message msg) {
        if(msg instanceof InMessage) {
        switch((InMessage)msg){
            case TIMEOUT:
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        alert.show();
                    }
                });
            break;
            case OKLOG:
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ViewManager.instance().changeView(Login.class);
                    }
                });
        }
    }}
    protected void homeStatusCheck() {
        if(HomeFactory.getHome().isEntered()) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ViewManager.instance().changeView(ControlManager.class);
                }
            });
        }
        if(HomeFactory.getHome().isLogged()) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ViewManager.instance().changeView(Enter.class);
                }
            });
        }

    }
}
