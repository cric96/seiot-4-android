package com.embedded.consegna4.view.common;


import com.embedded.consegna4.model.pattern.Message;

public interface NotifiableView {
    void processMsg(Message msg);
}
