package com.embedded.consegna4.view;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import com.embedded.consegna4.R;
import com.embedded.consegna4.model.home.Home;
import com.embedded.consegna4.model.home.HomeFactory;
import com.embedded.consegna4.model.home.InMessage;
import com.embedded.consegna4.model.home.TempMessage;
import com.embedded.consegna4.model.pattern.Message;
import com.embedded.consegna4.view.common.AbstractNotifiableActivity;
import com.embedded.consegna4.view.common.ViewManager;

public class ControlManager extends AbstractNotifiableActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.control_manager);
        final Home home = HomeFactory.getHome();
        final Button exit = this.findViewById(R.id.exit);
        final Thermometer thermometer = new Thermometer();
        thermometer.start();
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                home.disconnect();
                final Intent i = new Intent(getApplicationContext(),Welcome.class);
                startActivity(i);
                thermometer.cancel();
            }
        });
        final SeekBar seek = findViewById(R.id.lightIntensity);
        seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                home.setRange(seekBar.getProgress());
            }
        });
    }

    @Override
    protected void homeStatusCheck() {
        if(!HomeFactory.getHome().isLogged()) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ViewManager.instance().changeView(Login.class);
                }
            });
        }
        if(!HomeFactory.getHome().isEntered()) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ViewManager.instance().changeView(Enter.class);
                }
            });
        }
    }


    private class Thermometer extends Thread {
        private Boolean stopped = false;
        private final int delta = 5000;
        public void run() {
            while(!stopped) {
                HomeFactory.getHome().getTemperature();
                try {
                    Thread.sleep(delta);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        public void cancel() {
            this.stopped = true;
        }
    }

    @Override
    public void processMsg(Message msg) {
        Log.i("msg rcv", msg.toString());

        Log.i("is enter...",HomeFactory.getHome().isEntered()+"");
        if(msg instanceof TempMessage) {
            final int temp = ((TempMessage) msg).getTemp();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    TextView t = findViewById(R.id.temp);
                    Log.i("try to show:", ""+temp);
                    t.setText(getString(R.string.temperature) + "" + temp);
                }
            });
        } else if (msg instanceof InMessage) {
            switch ((InMessage)msg) {
                case EXIT:
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ViewManager.instance().changeView(Welcome.class);
                        }
                    });
            }
        }


    }
}
