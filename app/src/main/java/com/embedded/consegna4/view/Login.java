package com.embedded.consegna4.view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.embedded.consegna4.R;
import com.embedded.consegna4.model.home.Home;
import com.embedded.consegna4.model.home.HomeFactory;
import com.embedded.consegna4.model.home.InMessage;
import com.embedded.consegna4.model.pattern.Message;
import com.embedded.consegna4.view.common.AbstractNotifiableActivity;
import com.embedded.consegna4.view.common.ViewManager;

public class Login extends AbstractNotifiableActivity {
    private AlertDialog alertWrongCredential;
    private AlertDialog alertLongTimeout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Home home = HomeFactory.getHome();
        this.setContentView(R.layout.login);
        final Button login = findViewById(R.id.login);
        alertWrongCredential = new AlertDialog.Builder(this).create();
        alertWrongCredential.setTitle(getString(R.string.alertLoginTitle));
        alertWrongCredential.setMessage(getString(R.string.alertLoginMsg));

        alertLongTimeout = new AlertDialog.Builder(this).create();
        alertLongTimeout.setTitle(getString(R.string.loginDialogTitle));
        alertLongTimeout.setMessage(getString(R.string.loginDialogMsg));

        alertLongTimeout.setButton(Dialog.BUTTON_POSITIVE,getString(R.string.OkButton),new DialogInterface.OnClickListener(){

            @Override
            public void onClick(DialogInterface dialog, int which) {
                ViewManager.instance().changeView(Welcome.class);
                finish();
            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String user = ((TextView) findViewById(R.id.username)).getText().toString();
                String password = ((TextView) findViewById(R.id.password)).getText().toString();
                home.login(user,password);
            }
        });
    }
    @Override
    public void processMsg(Message msg) {
        if(msg instanceof InMessage) {
            switch((InMessage)msg) {
                case OKLOGGED:
                    Log.i("home",HomeFactory.getHome().isLogged()+"");
                    this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ViewManager.instance().changeView(Enter.class);
                        }
                    });
                    break;
                case WRONG_CREDENTIAL:
                    this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            alertWrongCredential.show();
                        }
                    });
                    break;
                case TIMEOUT:
                    this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            alertLongTimeout.show();
                        }
                    });
                    break;

            }
        }
    }
    @Override
    protected void homeStatusCheck() {
        if(HomeFactory.getHome().isEntered()) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ViewManager.instance().changeView(ControlManager.class);
                }
            });
        }
        if(HomeFactory.getHome().isLogged()) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ViewManager.instance().changeView(Enter.class);
                }
            });
        }
    }
}
