package com.embedded.consegna4.view.common;

import android.content.Intent;

import com.embedded.consegna4.model.pattern.Message;
import com.embedded.consegna4.model.pattern.Observer;
import com.embedded.consegna4.view.Welcome;

import java.util.Objects;

/**
 * a manager of view
 */
public class ViewManager implements Observer {
    private static final ViewManager SINGLETON = new ViewManager();

    public static final ViewManager instance() {
        return SINGLETON;
    }
    private ViewManager(){}
    private AbstractNotifiableActivity current;
    @Override
    public void notify(Message msg) {
        current.processMsg(msg);
    }

    public void changeView(final Class<?> activity) {
        Objects.requireNonNull(activity);
        final Intent i = new Intent(current.getApplicationContext(),activity);
        current.startActivity(i);
    }

    public void changeNotifiableView(final AbstractNotifiableActivity activity) {
        Objects.requireNonNull(activity);
        this.current = activity;
    }
}
