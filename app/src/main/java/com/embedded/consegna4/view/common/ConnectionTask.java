package com.embedded.consegna4.view.common;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.AsyncTask;
import android.widget.Button;

import com.embedded.consegna4.model.home.HomeFactory;

import java.io.IOException;
import java.util.UUID;


public final class ConnectionTask extends AsyncTask<Void,Void,Void>{

    private static final String GO_ON = "go on ...";
    private final Button btn;
    private boolean canEnable;
    private BluetoothSocket btSocket = null ;
    public ConnectionTask (final BluetoothDevice server , final UUID uuid, final Button btn ){
        this.btn = btn;
        try {
            btSocket = server.createRfcommSocketToServiceRecord ( uuid );
        } catch ( IOException e){ /* ... */ }
        this.canEnable = true;
    }
    @Override
    protected Void doInBackground ( Void ... params ){
        try{
            btSocket.connect ();
        } catch ( IOException connectException ){
            try{
                btSocket.close ();
                this.canEnable = false;
            } catch ( IOException closeException ){ /* ... */ }
            return null ;
        }
        try {
            HomeFactory.setChannel(btSocket);
        } catch (IOException e) {
            this.canEnable = false;
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void v) {
        if(this.canEnable) {
            this.btn.setText(GO_ON);
        }
        this.btn.setEnabled(this.canEnable);
    }
}
