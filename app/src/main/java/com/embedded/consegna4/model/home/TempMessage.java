package com.embedded.consegna4.model.home;

import com.embedded.consegna4.model.pattern.Message;

/**
 * a message that wrap temperature
 */
public class TempMessage implements Message {
    //costruttore privato poiché bisogna controllare che il messaggio venga creato da una stringa consona
    public static TempMessage build(String v) {
        String[] msg = v.split(";");
        if(msg[0].equals("T")) {
            return new TempMessage(Integer.parseInt(msg[1]));
        }
        return null;
    }
    private final int temp;

    private TempMessage(final int temp) {
        this.temp = temp;
    }

    public int getTemp() {
        return this.temp;
    }
}
