package com.embedded.consegna4.model.home;

import android.bluetooth.BluetoothSocket;

import com.embedded.consegna4.model.pattern.AbstractSource;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;

/**
 *
 */

public abstract class AbstractBluetoothHome extends AbstractSource implements Home , Runnable {
    private final static AbstractBluetoothHome HOME = new BluetoothHome();
    private BluetoothSocket i;

    private InputStream reader;
    private OutputStream print;
    public static AbstractBluetoothHome instance() {return HOME;}
    public void setChannel(BluetoothSocket i) throws IOException {
        this.i = i;
        this.reader = i.getInputStream();
        this.print = new PrintStream(i.getOutputStream());
    }

    protected InputStream input() {
        return reader;
    }

    protected OutputStream output() {
        return print;
    }
}
