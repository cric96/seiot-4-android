package com.embedded.consegna4.model.pattern;

/**
 * define a generic source
 */
public interface Source {
    /**
     * add observer to the source
     * @param o
     */
    void addObserver(final Observer o);
}
