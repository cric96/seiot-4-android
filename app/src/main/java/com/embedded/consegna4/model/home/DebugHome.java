package com.embedded.consegna4.model.home;

import android.util.Log;

import com.embedded.consegna4.model.pattern.AbstractSource;

import java.util.Random;

/**
 * Created by paggi on 31/03/2018.
 */

class DebugHome extends AbstractSource implements Home {
    private static final Home DEBUG = new DebugHome();
    static final Home instance() {return DEBUG;}
    private static final int SLEEP = 1000;
    private static final int MAX_DOUBLE = 100;
    final Random r = new Random();
    private DebugHome() {
        new Thread(new Runnable() {

            final int messages = InMessage.values().length;
            @Override
            public void run() {
                while(true) {
                    try {
                        Thread.sleep(3000);
                        DebugHome.super.sendMessage(InMessage.values()[r.nextInt(messages)]);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }
    @Override
    public void disconnect() {}

    @Override
    public void login(String user, String password) {

    }

    @Override
    public boolean isLogged() {
        return false;
    }

    @Override
    public boolean isEntered() {return false;}

    @Override
    public void setRange(int value) {
        Log.d("RANGE:",""+value);
    }

    @Override
    public void getTemperature() {

    }

    private void fakeWait() {
        try {
            Thread.sleep(SLEEP);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
