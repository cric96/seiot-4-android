package com.embedded.consegna4.model.home;

import com.embedded.consegna4.model.pattern.Source;

/**
 * an interface that describes a generic smart home
 */
public interface Home extends Source {
    /**
     * It finishes the work session
     */
    void disconnect();

    /**
     * try to connect to the home's system
     * @param user the username
     * @param password the password
     */
    void login(String user, String password);

    /**
     * define if the if the device is logged to the home or not
     * @return true if is logged, false otherwise
     */
    boolean isLogged();

    /**
     * tells if the person in entered in the home
     * @return true if it is in the home, false otherwise
     */
    boolean isEntered();
    /**
     * set the range to a specific actuator of the home
     * @throws new IllegalArgumentException if the value is less than 0 or greater than 100
     * @param value the value passed
     */

    void setRange(int value);

    /**
     * get the temperature of the home
     */
    void getTemperature();
}
