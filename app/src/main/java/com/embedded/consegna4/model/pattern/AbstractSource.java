package com.embedded.consegna4.model.pattern;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * define a skeleton of source
 */
public abstract class AbstractSource implements Source{
    private final Set<Observer> observers = new HashSet<>();

    /**
     * add observer to a generic source
     * @param o the observer
     */
    public final void addObserver(final Observer o) {
        Objects.requireNonNull(o);
        observers.add(o);
    }

    /**
     * send a message to all observer
     * @param msg the message to send
     */
    protected final void sendMessage(final Message msg) {
        Objects.requireNonNull(msg);
        for(final Observer o: observers) {
            o.notify(msg);
        }
    }
}
