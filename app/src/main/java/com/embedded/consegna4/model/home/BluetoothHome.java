package com.embedded.consegna4.model.home;

import android.util.Log;

import java.io.IOException;

//package-protected
class BluetoothHome extends AbstractBluetoothHome {
    private boolean logged;
    private boolean entered;
    //set false when the application has stopped
    private boolean stop;
    public BluetoothHome() {
        this.logged = false;
        this.entered = false;
        this.stop = false;
    }

    @Override
    public void disconnect() {
        try {
            output().write(("E").getBytes());
            Log.i("end","exit..");
        } catch (IOException e) {}
        this.logged = false;
        this.entered = false;
    }
    @Override
    public void login(String user, String password) {
        try {
            output().write(("L;"+user+";"+password).getBytes());
        } catch (IOException e) {}
    }

    @Override
    public boolean isLogged() {
        return this.logged;
    }

    @Override
    public boolean isEntered() {return this.entered;}

    @Override
    public void setRange(int value) {
        try {
            output().write(("V;"+value).getBytes());
        } catch (IOException e) {}
    }

    @Override
    public void getTemperature() {
        try {
            output().write("T".getBytes());
        } catch (IOException e) {}
    }

    public void run () {
        Log.i("WORK","on run");

        boolean msgCreated = false;
        String msg = "";
        while (!stop){
              try {
                Log.i("WORK","wait msg");
                char read = (char)input().read();
                Log.i("msg in byte","" + (read));
                if((read == '\n' || read == '\r')){
                  msgCreated = true;
                } else {
                  msg = msg + read;
                }
                if(msgCreated) {
                    Log.i("msg",""+msg);
                    broadcastMsg(msg);
                    msgCreated = false;
                    msg = "";
                }
            } catch ( IOException e) {
                stop = true ;
                Log.i("error","no read");
            }
        }
    }

    private void broadcastMsg(String msg) {
        Log.i("msg: ", msg);
        InMessage readMsg = InMessage.FACTORY.get(msg);
        if(readMsg == null) {
            TempMessage temp = TempMessage.build(msg);
            if(temp != null) {
                Log.i("temp read", msg);
                this.sendMessage(temp);
            }
        } else {
            switch (readMsg) {
                case ENTERED:
                    this.entered = true;
                    break;
                case EXIT:
                    this.entered = false;
                    this.logged = false;
                    break;
                case OKLOGGED:
                    Log.i("connected..","here");
                    this.logged = true;
                    Log.i("is logged??",this.logged + "");
                    break;
            }
            this.sendMessage(readMsg);
        }
    }
}
