package com.embedded.consegna4.model.home;

import com.embedded.consegna4.model.pattern.Message;

import java.util.HashMap;
import java.util.Map;

/**
 * defines what kind of message comes from Home (Arduino) to Mobile
 */
public enum InMessage implements Message {

    OKLOG, TIMEOUT, OKLOGGED, WRONG_CREDENTIAL, ENTERED, EXIT,NOT_DETECTED;
    /**
     * It associates a string to the corresponding Message
     */
    public static final Map<String,InMessage> FACTORY = new HashMap();
    static {
        FACTORY.put("L",OKLOG); //Posso loggarmi -> devo inviare le credenziali
        FACTORY.put("O",TIMEOUT); //tempo scaduto per effettuare un'azione o quando sono troppo lontano dalla porta
        FACTORY.put("A",OKLOGGED); //credenziali corrette
        FACTORY.put("N", WRONG_CREDENTIAL); //credenziali errate
        FACTORY.put("S", ENTERED); //entrato in casa, inizio della sessione
        FACTORY.put("F",EXIT); //pressione del pulsante exit su Arduino
        FACTORY.put("E",NOT_DETECTED); //se dopo aver effettuato il login non entro nella casa entro MAX_DELAY
    }
}
