package com.embedded.consegna4.model.pattern;

/**
 * define a generic observer
 */
public interface Observer {
    /**
     * process the msg received
     * @param msg the msg
     */
    void notify(Message msg);
}
