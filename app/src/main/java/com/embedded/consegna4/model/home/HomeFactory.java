package com.embedded.consegna4.model.home;

import android.bluetooth.BluetoothSocket;

import java.io.IOException;

/**
 * a factory used to create and return the single instance of home
 */
public class HomeFactory {
    private HomeFactory(){}
    private static Home HOME = null;

    public static boolean DEBUG = false;
    /**
     * get the home
     * @return a single instance of home
     */
    public static Home getHome() {
        if(HOME == null) {
            if(DEBUG) {
                initDebug();
            } else {
                initReal();
            }
        }
        return HOME;
    }

    /**
     * It is called from an activity to set the channel connection
     * @param bt in this case we will have a bluetooth channel
     * @throws IOException, if there are problems with the channel
     */
    public static void setChannel(BluetoothSocket bt) throws IOException {
        //Usata per effettuare il debug
        if(!DEBUG) {
            //cast usato per creare il thread e per settare il canale
            AbstractBluetoothHome home = (AbstractBluetoothHome)HOME;
            home.setChannel(bt);
            new Thread(home).start();
        }
    }
    private static final void initDebug() {HOME = DebugHome.instance();}
    private static final void initReal(){HOME = AbstractBluetoothHome.instance();}


}
